<?php

class Database{

    private static $connection = null;

    public static function connect(){
        try{
    
            self::$connection = new PDO ("mysql:host=localhost;dbname=burgercode","root","");
            self::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        }catch(PDOExeption $e){
            die($e->getMessage());
        }
        return self::$connection;
    } 

    public static function disconnect(){
        self::$connection = null;
    }
}

Database::connect();



?>
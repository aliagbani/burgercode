<?php
     
    require 'database.php';

    function checkInput($data) 
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    if(!empty($_GET['id'])){
        $id = checkInput($_GET['id']);
    }

    if(!empty($_POST)){
        $id = checkInput($_POST['id']);
        $db = Database::connect();
        $sql = 'DELETE FROM items WHERE id = '.$id;
        $db->exec($sql);
        //$statement = $db->prepare('DELETE FROM items WHERE id = ?');
        //$statement->execute(array($id));
        Database::disconnect();
        
        header("Location: index.php");
    }

?>



<!DOCTYPE html>
<html lang="fr">
<head> 
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Burger Code</title>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://kit.fontawesome.com/33b70fd194.js" crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css2?family=Holtwood+One+SC&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../assets/css/style.css">

</head>
<body> 
    <h1 class="text-logo">
        <span><i class="fas fa-utensils"></i></span> Burger Code <span><i class="fas fa-utensils"></i></span>
    </h1>
    <div class="container admin">
        <div class="row">
            <div class="col-md-6">
                <h1 class="text-center"><strong>Supprimer un item </strong></h1> <br>
                <form action="delete.php" method="post" class="form" role="form">
                    <div class="form-group">
                        <input type="hidden" name="id" value="<?= $id;?>"/>
                        <p class="alert alert-warning">Etes vous sur de vouloir supprimer ?</p>
                    </div>

                    <div class="form-actions  text-center">
                        <button type="submit" class="btn btn-danger"> Oui</button>
                        &nbsp;
                        <a href="index.php" class="btn btn-default"> Non</a> 
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="assets/js/script.js"></script>
</body>
</html>
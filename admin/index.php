<!DOCTYPE html>
<html lang="fr">
<head> 
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Burger Code</title>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://kit.fontawesome.com/33b70fd194.js" crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css2?family=Holtwood+One+SC&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../assets/css/style.css">

</head>
<body> 
    <h1 class="text-logo">
        <span><i class="fas fa-utensils"></i></span> Burger Code <span><i class="fas fa-utensils"></i></span>
    </h1>
    <div class="container admin">
        <div class="row">
            <h1><strong>Liste des items </strong> <a href="insert.php" class="btn btn-success btn-lg"><span><i class="fas fa-plus"></i></span> Ajouter</a>  </h1> 
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Description</th>
                        <th>Prix</th>
                        <th>Categorie</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        require 'database.php';
                        $db = Database::connect();
                        $req = "select i.id, i.name, i.description, i.price, c.name from items i left join categories c on i.category = c.id  ORDER by i.id desc";
                        $stat = $db->query($req);
                        //$items = $stat->fetchAll();
                        //foreach($items as $item)
                        while($item = $stat->fetch()){
                            echo'<tr>';
                                echo'<td style="width:130px">'. $item[1] .'</td>';
                                echo'<td>'. $item[2] .'</td>';
                                echo'<td style="width:90px">'. number_format($item[3], 2).' €</td>';
                                echo'<td>'. $item[4] .'</td>';
                                echo'<td style="width:300px">
                                    <a href="view.php?id='. $item[0] .'"class="btn btn-default"><span><i class="fas fa-eye"></i></span> Voir</a>
                                    <a href="update.php?id='. $item[0] .'" class="btn btn-primary"><span><i class="fas fa-pen"></i></span> Modifier</a>
                                    <a href="delete.php?id='. $item[0] .'" class="btn btn-danger"><span><i class="fas fa-trash-alt"></i></span> Supprimer </a>';
                                echo'</td>';
                            echo'</tr>';
                        }
                        
                    ?>
                </tbody>
            </table>
        </div>
    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="assets/js/script.js"></script>
</body>
</html>
<?php
     
    require 'database.php';    
    $nameError = $descriptionError = $priceError = $categoryError = $imageError = $name = $description = $price = $category = $image = "";

    if(!empty($_POST)){

        $name           = checkInput($_POST['name']);
        $description    = checkInput($_POST['description']);
        $price          = checkInput($_POST['price']);
        $category       = checkInput($_POST['category']);

        $image              = checkInput($_FILES["image"]["name"]);
        $imagePath          = '../assets/image/'. basename($image);
        $imageExtension     = pathinfo($imagePath,PATHINFO_EXTENSION);


        $isSuccess      = true;
        $isUploadSuccess = false;

        if(empty($name)){

            $nameError = 'Erreur - ce champ ne peut pas etre vide';
            $isSuccess = false;
        }
        
        if(empty($description)){

            $descriptionError = 'Erreur - ce champ ne peut pas etre vide';
            $isSuccess = false;
        }
        
        if(empty($price)){

            $priceError = 'Erreur - ce champ ne peut pas etre vide';
            $isSuccess = false;
        }

        if(empty($category)){

            $categoryError = 'Erreur - ce champ ne peut pas etre vide';
            $isSuccess = false;
        }

        if(empty($image)){

            $imageError = 'Erreur - ce champ ne peut pas etre vide';
            $isSuccess = false;
        }else{

            $isUploadSuccess = true;
            
            if($imageExtension!="jpg" && $imageExtension!="png" && $imageExtension!="jpeg" && $imageExtension!="gif"){

                $imageError = 'Erreur - les ficheirs autorises sont : .jpg, .png, .jpeg, .gif';
                $isUploadSuccess = false;
            }

            if(file_exists($imagePath)){
                $imageError = "Le fichier existe deja";
                $isUploadSuccess = false;
            }

            if($_FILES['image']['size'] > 500000){

                $imageError = 'Erreur - le ficheir ne doit pas depasser les 500KB';
                $isUploadSuccess = false;
            }
        }

        if($isSuccess && $isUploadSuccess){

            $db = Database::connect();
            //$sqlAjout = 'INSERT INTO items (name, description, price, image, category) VALUES ("'.$name.'", "'.$description.'", "'.$price.'", "'.$image.'", "'.$category.'" )';
            //$db->exec($sqlAjout);
            $req = "INSERT INTO items (name, description, price, image, category) VALUES (?, ?, ?, ?, ?)";
            $stat=$db->prepare($req);
            $stat->execute(array($name, $description, $price, $image, $category));
            Database::disconnect();
            header("Location: index.php");
        }
    }

    function checkInput($data){

        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
?>

<!DOCTYPE html>
<html lang="fr">
<head> 
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Burger Code</title>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://kit.fontawesome.com/33b70fd194.js" crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css2?family=Holtwood+One+SC&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../assets/css/style.css">

</head>
<body> 
    <h1 class="text-logo">
        <span><i class="fas fa-utensils"></i></span> Burger Code <span><i class="fas fa-utensils"></i></span>
    </h1>
    <div class="container admin">
        <div class="row">
            <div class="col-md-6">
            <h1 class="text-center"><strong>Ajouter un item </strong></h1> <br>
                <form action="insert.php" method="post" class="form" role="form" autocomplete="on" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="name">Nom</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Nom">
                        <span class='help-inline'><?=$nameError; ?></span>
                    </div>
                    
                    <div class="form-group">
                        <label for="description">Description</label>
                        <input type="text" class="form-control" id="description" name="description" placeholder="Description">
                        <span class='help-inline'><?=$descriptionError; ?></span>
                    </div>

                    <div class="form-group">
                        <label for="price">Prix</label>
                        <input type="number" step="0.01" class="form-control" id="price" name="price" placeholder="Prix">
                        <span class='help-inline'><?=$priceError; ?></span>
                    </div> 

                    <div class="form-group">
                        <label for="category">Categorie:</label> 
                        <select class="form-control" name="category" id="category" >
                            <?php
                            $db = Database::connect();
                            $req = "SELECT id, name FROM categories";
                            $stat = $db->query($req);
                            while($cat = $stat->fetch()){
                                echo'<option value="'.$cat[0].'">'.$cat[1].'</option>';
                            }
                            ?> 
                        </select>
                        <span class='help-inline'><?=$categoryError; ?></span>
                    </div> 
                    
                    <div class="form-group">
                        <label for="image">Choisissez une image</label>
                        <input type="file" id="image" name="image">
                        <span class='help-inline'><?=$imageError; ?></span>
                    </div>
                    <br>
                    <div class="form-actions  text-center">
                        <button type="submit" class="btn btn-success"><span><i class="fa fa-pencil"></i></span> Ajouter</button>
                        &nbsp;
                        <a href="index.php" class="btn btn-primary"><span><i class="fa fa-arrow-left"></i></span> Retour</a> 
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="assets/js/script.js"></script>
</body>
</html>
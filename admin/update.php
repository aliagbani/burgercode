<?php
     
    require 'database.php';
    $db = Database::connect();


    function checkInput($data){

        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    $nameError = $descriptionError = $priceError = $categoryError = $imageError = $name = $description = $price = $category = $image = "";

    if(!empty($_GET['id'])){

        $id = checkInput($_GET['id']);
    }

    if(!empty($_POST)){

        $name            = checkInput($_POST['name']);
        $description     = checkInput($_POST['description']);
        $price           = checkInput($_POST['price']);
        $category        = checkInput($_POST['category']);

        // Verifier: Image Extension + Image Size + Fichier Existe

        $image              = checkInput($_FILES["image"]["name"]);
        $imagePath          = '../assets/image/'. basename($image);
        $imageExtension     = pathinfo($imagePath,PATHINFO_EXTENSION);
        $isSuccess          = true;

        if(empty($name)){
            $nameError = 'Erreur - ce champ ne peut pas etre vide';
            $isSuccess = false;
        }

        if(empty($description)){
            $descriptionError = 'Erreur - ce champ ne peut pas etre vide';
            $isSuccess = false;
        }

        if(empty($price)){
            $priceError = 'Erreur - ce champ ne peut pas etre vide';
            $isSuccess = false;
        }

        if(empty($category)){
            $categoryError = 'Erreur - ce champ ne peut pas etre vide';
            $isSuccess = false;
        }

        if(empty($image)){

            $isImageUpdated  = false;

        }else{
            $isImageUpdated  = true;
            $isUploadSuccess = true;

            // Verifier: Image Extension + Image Size + Fichier Existe

            // 
            if($imageExtension != "jpg" && $imageExtension != "png" && $imageExtension != "jpeg" && $imageExtension != "gif"){

                $imageError = 'Erreur - les ficheirs autorises sont : .jpg, .png, .jpeg, .gif';
                $isUploadSuccess = false;
            }

            if($_FILES["image"]["size"] > 500000){

                $imageError = 'Erreur - le ficheir ne doit pas depasser 500KB';
                $isUploadSuccess = false;
            }

            if(file_exists($imagePath)){

                $imageError = 'Erreur - le ficheir deja existe';
                $isUploadSuccess = false;
            }
        }

      if (($isSuccess && $isUploadSuccess && $isImageUpdated) || ($isSuccess && !$isImageUpdated)){
            // requete sql
            if($isImageUpdated){

                $sql = 'UPDATE items SET name = "'.$name.'", description = "'.$description.'", price = "'.$price.'", image = "'.$image.'", category= "'.$category.'" WHERE id = "'.$id.'" ';
                $db->exec($sql);

            }else{

                $sql = ' UPDATE items  set name = "'.$name.'", description = "'.$description.'", price = "'.$price.'", category = "'.$category.'" WHERE id = "'.$id.'" ';
                $db->exec($sql);
            }

            
            header("Location: index.php");

            /* 
            // prepared Statment
            if($isImageUpdated){
                
                $reqUpdate = "UPDATE items  set name = ?, description = ?, price = ?, category = ?, image = ? WHERE id = ?";
                $statement = $db->prepare($reqUpdate);
                $statement->execute(array($name,$description,$price,$category,$image,$id));
            }else{

                $reqUpdate = "UPDATE items  set name = ?, description = ?, price = ?, category = ? WHERE id = ?";
                $statement = $db->prepare($reqUpdate);
                $statement->execute(array($name,$description,$price,$category,$id));
            }   
            header("Location: index.php");

            */

        }else if(!$isUploadSuccess && $isImageUpdated){

            $statement      = $db->prepare("SELECT * FROM items where id = ?");
            $statement->execute(array($id));
            $item           = $statement->fetch();
            $image          = $item['image'];

        }



    }else{
        
        $req = "SELECT i.id, i.name, i.description, i.price, c.name, i.image FROM items i , categories c WHERE i.category = c.id AND i.id = $id";
        $stat = $db->query($req);
        $item = $stat->fetch();

        $name = $item[1];
        $description = $item[2];
        $price = $item[3]; 
        $category = $item[4];
        $image = $item[5];
        
    }


?>

<!DOCTYPE html>
<html lang="fr">
<head> 
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Burger Code</title>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://kit.fontawesome.com/33b70fd194.js" crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css2?family=Holtwood+One+SC&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../assets/css/style.css">

</head>
<body> 
    <h1 class="text-logo">
        <span><i class="fas fa-utensils"></i></span> Burger Code <span><i class="fas fa-utensils"></i></span>
    </h1>
    <div class="container admin">
        <div class="row">
            <div class="col-sm-6">
                <h1><strong>Modifier un item</strong></h1>
                <form action="update.php?id=<?=$id?>" class="form" role="form" method="post" enctype="multipart/form-data">
                    <div class="from-group">
                        <label for="name">Nom:</label>
                        <input type="text" class="form-control" name="name" id="name" value="<?=$name?>">
                        <span class='help-inline'><?=$nameError; ?></span>
                    </div>

                    <div class="from-group">
                        <label for="description">Description:</label>
                        <input type="text" class="form-control" name="description" id="description" value="<?=$description?>">
                        <span class='help-inline'><?=$descriptionError; ?></span>
                    </div>
                    
                    <div class="from-group">
                        <label for="price">Prix: (en €)</label>
                        <input type="number" step="0.01" class="form-control" name="price" id="price" value="<?=number_format($price, 2)?>">
                        <span class='help-inline'><?=$priceError; ?></span>
                    </div>
                    
                    <div class="from-group">
                        <label for="category">Categorie: </label>
                        <select class="form-control" name="category" id="category">
                            <?php
                                
                                $req2 = "SELECT * FROM categories";
                                $stat2 = $db->query($req2);
                                $categories = $stat2->fetchAll();
                                foreach($categories as $categ){

                                    if($categ[1] == $category){

                                        echo '<option value="'. $categ[0] .'" selected>'. $categ[1] .'</option>';
                                    }else{

                                        echo '<option value="'. $categ[0] .'">'. $categ[1] .'</option>';
                                    }
                                }
                            ?>
                            
                        </select>
                        <span class='help-inline'><?=$categoryError; ?></span>
                    </div>
                    
                    <div class="from-group">
                        <label>Image: </label>
                        <p class="form-control" disabled><?=$image?></p>
                    </div>
                    
                    <div class="from-group">
                        <label for="image">Selectionner une nouvelle image: </label>
                        <input class="form-control" type="file" name="image" id="image">
                        <span class='help-inline'><?=$imageError; ?></span>
                    </div>
                    <br>
                    <div class="form-actions text-center">
                        <button type="submit" class="btn btn-success"><span><i class="fa fa-pencil"></i></span> Modifier</button>
                        &nbsp;
                        <a href="index.php" class="btn btn-primary"><span><i class="fa fa-arrow-left"></i></span> Retour</a> 
                    </div>
                </form>
            </div>
            <div class="col-sm-6">
               <div class="thumbnail">
                    <img src="../assets/image/<?=$image?>" alt="..." >
                    <div class="price"><?=number_format($price, 2)?> $</div>
                    <div class="caption">
                        <h4><?=$name?></h4>
                        <p><?=$description?></p>
                        <a href="#" class="btn btn-order" role="button"><i class="fas fa-shopping-cart"></i> Commander</a>
                    </div>
               </div> 
            </div>
        </div>
    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="assets/js/script.js"></script>
</body>
</html>

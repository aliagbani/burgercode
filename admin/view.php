<?php
    require 'database.php';
    
    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    if(!empty($_GET['id'])){
        $id = test_input($_GET['id']);
    }

    $db = Database::connect();
    $req = "select i.id, i.name, i.description, i.price, i.image, c.name from items i , categories c WHERE i.category = c.id and i.id = " .$id. " ORDER by i.id DESC";
    $stat = $db->query($req);
    // $items = $stat->fetchAll();
    // foreach( $items as $item)
    $item = $stat->fetch();
//    Database::dicconnect();

    
?>

<!DOCTYPE html>
<html lang="fr">
<head> 
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Burger Code</title>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://kit.fontawesome.com/33b70fd194.js" crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css2?family=Holtwood+One+SC&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../assets/css/style.css">

</head>
<body> 
    <h1 class="text-logo">
        <span><i class="fas fa-utensils"></i></span> Burger Code <span><i class="fas fa-utensils"></i></span>
    </h1>
    <div class="container admin">
        <div class="row">
            <div class="col-sm-6">
                <h1><strong>Voir des items </strong></h1><br>
                <form>
                    <div class="form-groupe">
                        <label>Nom: </label> <?= $item[1]?>
                    </div><br>
                    <div class="form-groupe">
                        <label>Description: </label> <?= $item[2]?>
                    </div><br>
                    <div class="form-groupe">
                        <label>Prix: </label> <?= number_format($item[3],2)?> $
                    </div><br>
                    <div class="form-groupe">
                        <label>Categorie: </label> <?= $item[5]?>
                    </div><br>
                    <div class="form-groupe">
                        <label>Image: </label> <?= $item[4]?>
                    </div><br>
                </form>
                <a href="index.php" class="btn btn-primary btn-lg"><span><i class="fa fa-arrow-left"></i></span> Retour</a>
            </div>

            <div class="col-sm-6">
                <div class="thumbnail">
                    <img src="../assets/image/<?= $item[4]?>" alt="..." >
                    <div class="price"><?= number_format($item[3],2)?> $</div>
                    <div class="caption">
                        <h4><?= $item[1]?></h4>
                        <p><?= $item[2]?></p>
                        <a href="#" class="btn btn-order" role="button"><i class="fas fa-shopping-cart"></i> Commander</a>
                    </div>
                </div>
            
            </div>
        </div>
    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="assets/js/script.js"></script>
</body>
</html>
<?php
require 'admin/database.php'; 
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Burger Code</title>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://kit.fontawesome.com/33b70fd194.js" crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css2?family=Holtwood+One+SC&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">

</head>
<body> 
    <div class="container site">
        <h1 class="text-logo">
            <span><i class="fas fa-utensils"></i></span> Burger Code <span><i class="fas fa-utensils"></i></span>
        </h1>
        
        <nav>
            <ul class="nav nav-pills">
            <?php
                $db = Database::connect();
                $sqlCat = "select * from categories";
                $stat = $db->query($sqlCat);
                $listeCats = $stat->fetchAll();
                foreach($listeCats as $uneCat){
                    if($uneCat[0] == 1){

                        echo ' <li role="presentation" class="active"> <a href="#'.$uneCat[0].'" data-toggle="tab"> '.$uneCat[1].' </a></li> ';
                    }
                    else{
                        echo ' <li role="presentation"> <a href="#'.$uneCat[0].'" data-toggle="tab"> '.$uneCat[1].' </a></li> ';
                    }
                    

                }
            
                
            echo'</ul>';
        echo ' </div>';

    echo'<div class="tab-content">';
        
        // id = 1 
        
            foreach($listeCats as $uneCat){
                if($uneCat[0] == 1){

                    echo '<div class="tab-pane active" id="'.$uneCat[0].'">';
                }
                else{
                    echo '<div class="tab-pane" id="'.$uneCat[0].'">';                
                }

                
                    echo'<div class="row">';
                    
                    $sqlItems = 'SELECT id, name, description, price, image, category FROM items WHERE items.category = '.$uneCat[0];
                    $statItems = $db->query($sqlItems);
                    $listeItems = $statItems->fetchAll();

                    foreach($listeItems as $item){

                        echo '<div class="col-sm-6 col-md-4">';
                            echo '<div class="thumbnail">';
                                echo ' <img src="assets/image/'.$item[4].'" alt="..." >';
                                echo ' <div class="price">'.number_format($item[3],2).' $</div>';
                                echo ' <div class="caption">';
                                    echo ' <h4> '.$item[1].'</h4>';
                                    echo ' <p> '.$item[2].' </p>';
                                    echo ' <a href="#" class="btn btn-order" role="button"><i class="fas fa-shopping-cart"></i> Commander</a>';
                                echo ' </div>';
                            echo '</div>';
                        echo '</div>';
                    }
                    echo'</div>';
                echo'</div>';
            }
            $db = Database::disconnect();    
        ?> 
    </div>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="assets/js/script.js"></script>
</body>
</html>